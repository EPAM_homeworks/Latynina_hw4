def mysqrt(n, epsilon=0):
    if epsilon != 0:
        s = str(epsilon).split('.')
        if len(s) == 2:
            round_number = len(s[1])
        else:
            round_number = int(s[0].strip()[-1])
    prev, x = -1, 1
    
    while abs(x - prev) > epsilon:
        prev = x
        x = x - (x*x - n) / (2*x)
    
    return (x if not epsilon else round(x, round_number))

assert mysqrt(4) == 2
assert mysqrt(5,0.01) == 2.24
assert mysqrt(4/10000) == 0.02

print(mysqrt(4/10000, 0.001))
print(mysqrt(4/1000000, 0.001))
print(mysqrt(4/100000000, 0.001))
print(mysqrt(624,0.00001))