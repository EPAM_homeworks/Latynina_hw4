import time
import functools
import numpy as np

def make_cache(func):
    cache = {}
    @functools.wraps(func)
    def wrapper(*fargs):
        if fargs not in cache:
            value = func(*fargs)
            cache[fargs] = value
            return value
        else:
            return cache[fargs]
    return wrapper
            
def time_counter(func=None, *, var='time and calls'):
    if func is None:
        return lambda func: time_counter(func, var=var)
    globals()[var]=[0,0]
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        nonlocal var
        start_time = time.time()
        worker = func(*args, **kwargs)
        elapsed_time = time.time() - start_time
        previous = globals().get(var)
        globals()[var] = list(map(lambda x,y: x+y, previous, [elapsed_time, 1]))
        return worker
    return wrapper
    
    
@time_counter(var='fib_cached')
@make_cache
def fib(n):
    if n < 2 : return n
    return fib(n-1) + fib(n-2)

@time_counter(var='golden_fib_')
def golden_fib(n):
    phi = (1 + 5**(1/2))/2
    return round(phi**n / 5**(1/2))


@time_counter(var='fib_2_')
def fib_2(i, current = 0, next = 1):
    if i == 0:
        return current
    else:
        return fib_2(i - 1, next, current + next)
        
funcs = [fib, fib_2, golden_fib]
func_vars = ['fib_cached', 'fib_2_', 'golden_fib_']

def find_optimal(funcs, func_vars):
    results = []
    for func in funcs:
        results.append(func(300))
    if results[-1] != results[0] and results[-1] != results[1]:
        print(f'{func_vars[-1]} does not work correctly')
        del results[-1]
        del func_vars[-1]
    competition = []
    for name in func_vars:
        competition.append(globals().get(name))
    times = [x[0] for x in competition]
    calls = [x[1] for x in competition]

    time_winner = min(times)
    calls_winner = min(calls)
    for i in range(len(times)):
        if times[i] == time_winner:
            print(f'Time-winner is {func_vars[i]} with score {time_winner}')
        if calls[i] == calls_winner:
            print(f'Calls-winner is {func_vars[i]} with score {calls_winner}')
            
find_optimal(funcs, func_vars)