def gcd(a, b):
    if b == 0:
        return a;
    return gcd(b, a%b)

def count_on_edges(a, b):
    if a[0] == b[0]:
        return abs(a[1] - b[1]) + 1
    if a[1] == b[1]:
        return abs(a[0] - b[0]) + 1
    diff = list(map(lambda x,y: abs(x-y), a,b))
    div = gcd(diff[0], diff[1])
    sl = (diff[1] // div)
    return (diff[1] // sl or 1) + 1


def count_points(a, b, c):
    coordinates, edges_points = [a, b, c, a], 0
    area = abs((a[0]*(b[1]-c[1]) + b[0]*(c[1] - a[1]) + c[0]*(a[1] - b[1]))/2)
    for i in range(3):
            edges_points += count_on_edges(coordinates[i], coordinates[i+1])
    inside_points = (area+1+(edges_points-3)/2)
    return round(inside_points)


print(count_points((2, 1), (5, 5), (7, 3)))
print(count_points((-2, -5), (0, 0), (5, 2)))
print(count_points((5, 2), (0, 0), (-2, -5)))
print(count_points((5, 2), (-2, -5), (0, 0)))
print(count_points((-2, -2), (0, 4), (4, 3)))
print(count_points((-2, -2), (0, 4), (3, 5)))
print(count_points((4, -1), (6, 3), (0, -5)))
print(count_points((0,0),(2,2),(4,4)))