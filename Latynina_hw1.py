def func(data=None):
    
    if data != None:
        if data == 'cancel':
            return 'Bye!'
        if type(data) == str and data.isdecimal():
            return func(int(data))
        if type(data) == int and data%2 == 0:
            return int(data/2)
        if type(data) == int and data%2 != 0:
            return data*3+1

        else:
            return 'Не удалось преобразовать введенный текст в число.'

def call():
    data = 0
    while True:
        data = input()
        print(func(data))
        if data == 'cancel':
            break
call()