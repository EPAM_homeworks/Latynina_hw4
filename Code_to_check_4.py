sites = ['https://stackoverflow.com', 'https://www.youtube.com/', 'https://docs.python.org/', 'https://github.com/']
 
@make_cache_timed(3)
def check_code(site):
    try:
        r = requests.head(site)
        return r.status_code, r.url
    except requests.ConnectionError:
        print("failed to connect")
for site in sites:
    print(check_code(site))
sleep(1)
for site in sites:
    print(check_code(site))