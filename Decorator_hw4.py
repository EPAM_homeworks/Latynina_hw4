import functools
from time import time, sleep
import requests

def make_cache_timed(t=10):
    cache={}
    def decorator(func):
        name = func.__name__
        cache[name] = {}
        @functools.wraps(func)
        def wrapper(*args):
            if args in cache[name]:
                if time() - cache[name][args][1] > t:
                    return cache[name].pop(args)[0]
                else:
                    return cache[name][args][0]
            else:
                val = func(*args)
                cache[name][args] = (val, time())
                return val
        return wrapper
    return decorator